sphere {
	0, (0.8 * clock - 0.4) + 1
	translate y * (1.4 + clock/2 - 1)
	rotate <65.0743, 360, 0>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.96875
	translate y * (1.4 + clock/2 - 0.96875)
	rotate <249.567, 348.75, 11.25>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.9375
	translate y * (1.4 + clock/2 - 0.9375)
	rotate <32.0097, 337.5, 22.5>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.90625
	translate y * (1.4 + clock/2 - 0.90625)
	rotate <257.318, 33.75, 326.25>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.875
	translate y * (1.4 + clock/2 - 0.875)
	rotate <178.134, 45, 315>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.84375
	translate y * (1.4 + clock/2 - 0.84375)
	rotate <303.75, 56.25, 57.0645>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.8125
	translate y * (1.4 + clock/2 - 0.8125)
	rotate <278.417, 292.5, 67.5>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.78125
	translate y * (1.4 + clock/2 - 0.78125)
	rotate <80.644, 78.75, 281.25>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.75
	translate y * (1.4 + clock/2 - 0.75)
	rotate <270, 90, 201.388>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.71875
	translate y * (1.4 + clock/2 - 0.71875)
	rotate <258.75, 101.25, 108.046>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.6875
	translate y * (1.4 + clock/2 - 0.6875)
	rotate <89.7571, 112.5, 247.5>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.65625
	translate y * (1.4 + clock/2 - 0.65625)
	rotate <42.3638, 123.75, 236.25>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.625
	translate y * (1.4 + clock/2 - 0.625)
	rotate <225, 135, 253.842>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.59375
	translate y * (1.4 + clock/2 - 0.59375)
	rotate <213.75, 146.25, 115.541>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.5625
	translate y * (1.4 + clock/2 - 0.5625)
	rotate <153.208, 157.5, 202.5>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.53125
	translate y * (1.4 + clock/2 - 0.53125)
	rotate <218.619, 191.25, 168.75>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.5
	translate y * (1.4 + clock/2 - 0.5)
	rotate <318.325, 180, 180>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.46875
	translate y * (1.4 + clock/2 - 0.46875)
	rotate <186.944, 168.75, 191.25>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.4375
	translate y * (1.4 + clock/2 - 0.4375)
	rotate <117.211, 202.5, 157.5>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.40625
	translate y * (1.4 + clock/2 - 0.40625)
	rotate <274.117, 146.25, 213.75>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.375
	translate y * (1.4 + clock/2 - 0.375)
	rotate <148.542, 225, 135>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.34375
	translate y * (1.4 + clock/2 - 0.34375)
	rotate <139.296, 236.25, 123.75>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.3125
	translate y * (1.4 + clock/2 - 0.3125)
	rotate <332.604, 247.5, 112.5>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.28125
	translate y * (1.4 + clock/2 - 0.28125)
	rotate <155.383, 258.75, 101.25>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.25
	translate y * (1.4 + clock/2 - 0.25)
	rotate <4.31558, 270, 90>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.21875
	translate y * (1.4 + clock/2 - 0.21875)
	rotate <148.935, 281.25, 78.75>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.1875
	translate y * (1.4 + clock/2 - 0.1875)
	rotate <67.5, 292.5, 49.3304>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.15625
	translate y * (1.4 + clock/2 - 0.15625)
	rotate <143.106, 303.75, 56.25>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.125
	translate y * (1.4 + clock/2 - 0.125)
	rotate <45, 315, 318.364>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.09375
	translate y * (1.4 + clock/2 - 0.09375)
	rotate <188.539, 326.25, 33.75>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.0625
	translate y * (1.4 + clock/2 - 0.0625)
	rotate <229.055, 337.5, 22.5>
}
sphere {
	0, (0.8 * clock - 0.4) + 0.03125
	translate y * (1.4 + clock/2 - 0.03125)
	rotate <343.159, 348.75, 11.25>
}
