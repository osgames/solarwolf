
camera {
	orthographic
	up y*(YMAX - YMIN)
	right x*(XMAX - XMIN)
	location <(XMAX + XMIN)/2,(YMAX + YMIN)/2,-15>
	look_at  <(XMAX + XMIN)/2,(YMAX + YMIN)/2,0>
}
