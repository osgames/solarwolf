
light_source {
	<-4, 20, -12>
	color rgb <1,1,1>
	area_light
	20*x, 20*y, 5,5
	adaptive 4
	circular orient
}

light_source {
	<6, 1, -15>
	color rgb <0.5, 0.5, 0.5>
}

light_source {
	<3, -16, 7>
	color rgb <1.0, 0.9, 0.8>
	area_light
	10*x, 10*y, 5,5
	adaptive 4
	circular orient
}
